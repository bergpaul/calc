use assert_cmd::prelude::*;
use predicates::str::{contains};
use std::process::Command;

#[test]
fn test_wrong_assignment() {
    Command::cargo_bin("calc")
        .unwrap()
        .args(&["-c", "1 = 1"])
        .assert()
        .stdout(contains("Expected variable name but got"));
}

#[test]
fn test_variable_unkown() {
    Command::cargo_bin("calc")
        .unwrap()
        .args(&["-c", "a"])
        .assert()
        .stdout(contains("Variable a not found"));
}

#[test]
fn test_precedence() {
    Command::cargo_bin("calc")
        .unwrap()
        .args(&["-c", "1 / 2 * 2"])
        .assert()
        .stdout(contains("= 1"));
}
