# Calc

A cli utility to perform calculations and unit transformations ensuring unit homogeneity.

## Usage

 - [x] Unit casting
```
12 m -> km
1 kg -> lbs
```
 - [x] Calculations
```
12 s + 3 min -> h
```
 - [x] Variable declaration
```
a = 12 m/s^2
m = 20 kg
```
 - [ ] Math functions
```
sqrt(1 m/s)
```

- [ ] Syntax coloring
- [ ] Better error handling
- [ ] Non unicode character
- [ ] Built-in constants (pi, tau, etc...)
