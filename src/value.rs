use std::cmp::{Eq, PartialEq};
use std::string::ToString;

use crate::unit::Unit;
use crate::{CalcError, Result};

#[derive(Debug, Clone)]
pub struct Value {
    val: f64,
    pub unit: Unit,
}

impl ToString for Value {
    fn to_string(&self) -> String {
        format!("{}{}", self.val, self.unit.to_string())
    }
}

impl Value {
    pub fn new(val: f64, unit: Unit) -> Value {
        Value { val, unit }
    }

    pub fn empty(val: f64) -> Value {
        Value {
            val,
            unit: Unit::Empty,
        }
    }

    pub fn to_unit(&self, unit: Unit) -> Value {
        let new_val = self.val * self.unit.to_base() * unit.from_base();
        Value::new(new_val, unit)
    }

    pub fn set_val(&mut self, val: f64) {
        self.val = val;
    }

    pub fn add(self, other: Value) -> Result<Value> {
        if self.unit != other.unit {
            Err(CalcError::Error(format!(
                "Unit {} and {} are not homogenous",
                self.unit.to_string(),
                other.unit.to_string()
            )))
        } else {
            let mut cast = other.to_unit(self.unit);
            cast.set_val(cast.val + self.val);
            Ok(cast)
        }
    }

    pub fn minus(self, other: Value) -> Result<Value> {
        if self.unit != other.unit {
            Err(CalcError::Error(format!(
                "Unit {} and {} are not homogenous",
                self.unit.to_string(),
                other.unit.to_string()
            )))
        } else {
            let mut cast = other.to_unit(self.unit);
            cast.set_val(self.val - cast.val);
            Ok(cast)
        }
    }

    pub fn imul(&mut self, factor: f64) {
        self.val *= factor;
    }

    pub fn idiv(&mut self, factor: f64) {
        self.val /= factor;
    }

    pub fn mul(self, other: Value) -> Result<Value> {
        let new_unit = Unit::times(self.unit, other.unit);
        let new_val = self.val * other.val;

        Ok(Value::new(new_val, new_unit))
    }

    pub fn div(self, other: Value) -> Result<Value> {
        let new_unit = Unit::div(self.unit, other.unit);
        if other.val == 0. {
            Err(CalcError::Error("Dividend is equal to zero".to_string()))
        } else {
            let new_val = self.val / other.val;

            Ok(Value::new(new_val, new_unit))
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Value) -> bool {
        self.val * self.unit.to_base() == other.val * other.unit.to_base()
            && self.unit == other.unit
    }
}

impl Eq for Value {}

#[test]
fn test_value_eq() {
    let a = Value::new(
        5.,
        Unit::NonStandard(
            "km".to_string(),
            Box::new(Unit::Standard("m".to_string())),
            1000.0,
        ),
    );
    let b = Value::new(5000., Unit::Standard("m".to_string()));

    assert_eq!(a, b);
}

#[test]
fn test_unit_conversion() {
    let a = Value::new(5., Unit::Standard("m".to_string()));
    let km = Unit::NonStandard(
        "km".to_string(),
        Box::new(Unit::Standard("m".to_string())),
        1000.,
    );

    let conversion = a.to_unit(km);

    assert_eq!(conversion.val, 0.005);
    assert_eq!(conversion, a);
}
