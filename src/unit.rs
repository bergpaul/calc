use std::cmp::{Eq, PartialEq};
use std::collections::HashMap;
use std::string::ToString;

#[derive(Debug, Clone)]
pub enum Unit {
    Empty,
    Standard(String),
    NonStandard(String, Box<Unit>, f64),
    Times(Box<Unit>, Box<Unit>),
    Div(Box<Unit>, Box<Unit>),
}

impl PartialEq for Unit {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Unit::Empty => match other {
                Unit::Empty => true,
                _ => false,
            },
            Unit::Standard(symbol) => match other.get_symbol() {
                Some(other_symbol) => symbol.to_string() == other_symbol,
                None => false,
            },
            Unit::NonStandard(_, unit, _) => match &**unit {
                Unit::Standard(symbol) => match other.get_symbol() {
                    Some(other_symbol) => symbol.to_string() == other_symbol,
                    None => false,
                },
                _ => &**unit == other,
            },
            Unit::Times(left, right) => match other {
                Unit::Times(other_left, other_right) => {
                    (&**left == &**other_left && &**right == &**other_right)
                        || (&**left == &**other_right && &**right == &**other_left)
                }
                Unit::NonStandard(_, unit, _) => match &**unit {
                    Unit::Times(other_left, other_right) => {
                        (&**left == &**other_left && &**right == &**other_right)
                            || (&**left == &**other_right && &**right == &**other_left)
                    }
                    _ => false,
                },
                _ => false,
            },
            Unit::Div(upper, lower) => match other {
                Unit::Div(other_upper, other_lower) => other_upper == upper && other_lower == lower,
                Unit::NonStandard(_, unit, _) => match &**unit {
                    Unit::Div(other_upper, other_lower) => {
                        other_upper == upper && other_lower == lower
                    }
                    _ => false,
                },
                _ => false,
            },
        }
    }
}

impl Eq for Unit {}

impl ToString for Unit {
    fn to_string(&self) -> String {
        match self {
            Unit::Empty => "".to_string(),
            Unit::Standard(symbol) => symbol.to_string(),
            Unit::NonStandard(symbol, ..) => symbol.to_string(),
            Unit::Times(left, right) => format!("{}.{}", left.to_string(), right.to_string()),
            Unit::Div(up, down) => format!("{}/{}", up.to_string(), down.to_string()),
        }
    }
}

impl Unit {
    pub fn div(up: Unit, down: Unit) -> Unit {
        if up == down {
            Unit::Empty
        } else {
            match down {
                Unit::Empty => up,
                _ => Unit::Div(Box::new(up), Box::new(down)),
            }
        }
    }

    pub fn times(left: Unit, right: Unit) -> Unit {
        match left {
            Unit::Empty => right,
            _ => match right {
                Unit::Empty => left,
                _ => Unit::Times(Box::new(left), Box::new(right)),
            },
        }
    }

    fn get_symbol(&self) -> Option<String> {
        match self {
            Unit::Standard(symbol) => Some(symbol.to_string()),
            Unit::NonStandard(_, unit, _) => unit.get_symbol(),
            _ => None,
        }
    }

    pub fn from_base(&self) -> f64 {
        match self {
            Unit::NonStandard(_, _, ratio) => 1. / ratio,
            Unit::Empty | Unit::Standard(..) => 1.,
            Unit::Times(left, right) => left.from_base() * right.to_base(),
            Unit::Div(up, down) => up.from_base() / down.from_base(),
        }
    }

    pub fn to_base(&self) -> f64 {
        match self {
            Unit::NonStandard(_, _, ratio) => *ratio,
            Unit::Empty | Unit::Standard(..) => 1.,
            Unit::Times(left, right) => 1. / (left.to_base() * right.to_base()),
            Unit::Div(up, down) => up.to_base() / down.to_base(),
        }
    }
}

fn standard(short_name: &str) -> Unit {
    Unit::Standard(short_name.to_string())
}

fn non_standard(short_name: &str, unit: Unit, ratio: f64) -> Unit {
    let unit = Box::new(unit);
    Unit::NonStandard(short_name.to_string(), unit, ratio)
}

#[derive(Debug)]
pub struct UnitRegister {
    units: HashMap<String, Unit>,
}

impl UnitRegister {
    pub fn new() -> UnitRegister {
        let mut register = UnitRegister {
            units: HashMap::new(),
        };
        register.register("m", standard("m"));
        register.register("cm", non_standard("cm", standard("m"), 0.01));
        register.register("mm", non_standard("mm", standard("m"), 0.001));
        register.register("km", non_standard("km", standard("m"), 1000.));
        register.register("s", standard("s"));
        register.register("ms", non_standard("ms", standard("s"), 0.001));
        register.register("min", non_standard("min", standard("s"), 60.0));
        register.register("h", non_standard("h", standard("s"), 3600.0));
        register.register("day", non_standard("day", standard("s"), 86400.));
        register.register("DOL", standard("DOL"));
        register
    }

    fn register(&mut self, symbol: &str, unit: Unit) {
        self.units.insert(symbol.to_string(), unit);
    }

    pub fn get(&self, symbol: &String) -> Option<Unit> {
        self.units.get(symbol).cloned()
    }
}

#[test]
fn test_equal_std() {
    let a = Unit::Standard("s".to_string());
    let b = Unit::Standard("m".to_string());

    assert_eq!(a == b, false);
}

#[test]
fn test_equal_non_std() {
    let a = Unit::Standard("s".to_string());
    let b = Unit::NonStandard(
        "min".to_string(),
        Box::new(Unit::Standard("s".to_string())),
        60.0,
    );

    assert_eq!(a == b, true);
}

#[test]
fn test_equal_times() {
    let a = Unit::NonStandard(
        "min".to_string(),
        Box::new(Unit::Times(
            Box::new(Unit::Standard("s".to_string())),
            Box::new(Unit::Standard("m".to_string())),
        )),
        60.0,
    );
    let b = Unit::NonStandard(
        "foo".to_string(),
        Box::new(Unit::Times(
            Box::new(Unit::Standard("s".to_string())),
            Box::new(Unit::Standard("m".to_string())),
        )),
        200.0,
    );
    let c = Unit::Times(
        Box::new(Unit::Standard("s".to_string())),
        Box::new(Unit::Standard("m".to_string())),
    );

    assert_eq!(a == b, true);
    assert_eq!(b == c, true);
}

#[test]
fn test_equal_div() {
    let a = Unit::NonStandard(
        "foo".to_string(),
        Box::new(Unit::Div(
            Box::new(Unit::Standard("s".to_string())),
            Box::new(Unit::Standard("m".to_string())),
        )),
        100.0,
    );
    let b = Unit::NonStandard(
        "bar".to_string(),
        Box::new(Unit::Div(
            Box::new(Unit::Standard("s".to_string())),
            Box::new(Unit::Standard("m".to_string())),
        )),
        200.0,
    );
    let c = Unit::Div(
        Box::new(Unit::Standard("s".to_string())),
        Box::new(Unit::Standard("m".to_string())),
    );

    assert_eq!(a == b, true);
    assert_eq!(b == c, true);
}

#[test]
fn test_unit_readable_symbol() {
    let a = Unit::Div(
        Box::new(Unit::Standard("m".to_string())),
        Box::new(Unit::Standard("s".to_string())),
    );
    let b = Unit::Times(
        Box::new(Unit::Standard("m".to_string())),
        Box::new(Unit::Standard("s".to_string())),
    );
    let c = Unit::Standard("m".to_string());
    let d = Unit::NonStandard(
        "mile".to_string(),
        Box::new(Unit::Standard("m".to_string())),
        1952.,
    );

    assert_eq!(a.to_string(), "m/s".to_string());
    assert_eq!(b.to_string(), "m.s".to_string());
    assert_eq!(c.to_string(), "m".to_string());
    assert_eq!(d.to_string(), "mile".to_string());
}

#[test]
fn test_from_base_ratios() {
    let km = Unit::NonStandard(
        "km".to_string(),
        Box::new(Unit::Standard("m".to_string())),
        1000.,
    );
    let m = Unit::Standard("m".to_string());
    let ms = Unit::Div(
        Box::new(Unit::NonStandard(
            "km".to_string(),
            Box::new(Unit::Standard("m".to_string())),
            1000.,
        )),
        Box::new(Unit::NonStandard(
            "h".to_string(),
            Box::new(Unit::Standard("s".to_string())),
            3600.,
        )),
    );

    assert_eq!(km.from_base(), 1. / 1000.);
    assert_eq!(m.from_base(), 1.);
    assert_eq!(ms.from_base(), 3.6);
}

#[test]
fn test_to_base_ratios() {
    let km = Unit::NonStandard(
        "km".to_string(),
        Box::new(Unit::Standard("m".to_string())),
        1000.,
    );
    let m = Unit::Standard("m".to_string());
    let ms = Unit::Div(
        Box::new(Unit::NonStandard(
            "km".to_string(),
            Box::new(Unit::Standard("m".to_string())),
            1000.,
        )),
        Box::new(Unit::NonStandard(
            "h".to_string(),
            Box::new(Unit::Standard("s".to_string())),
            3600.,
        )),
    );

    assert_eq!(km.to_base(), 1000.);
    assert_eq!(m.to_base(), 1.);
    assert_eq!(ms.to_base(), 1. / 3.6);
}
