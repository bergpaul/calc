pub mod lexer;
pub mod parser;
pub mod unit;
mod value;
mod variable;

use lexer::lex;
use parser::{parse, GrammarItem};
use unit::UnitRegister;
use value::Value;
use variable::VariableRegister;

#[derive(Debug)]
pub enum CalcError {
    PlacedError(String, usize, usize),
    Error(String),
}

pub type Result<T> = std::result::Result<T, CalcError>;

pub struct Interpreter {
    unit_register: UnitRegister,
    variable_register: VariableRegister,
}

impl Interpreter {
    pub fn new() -> Interpreter {
        Interpreter {
            unit_register: UnitRegister::new(),
            variable_register: VariableRegister::new(),
        }
    }

    pub fn step(&mut self, input: &String) -> Result<()> {
        let tokens = lex(input)?;
        if tokens.len() == 0 {
            return Ok(());
        }
        let ast = parse(&self.variable_register, &self.unit_register, &tokens)?;
        println!("{}", ast.to_string());

        let result = match ast.entry {
            GrammarItem::Assignment => {
                let value = ast.right.unwrap().evaluate()?;

                match ast.left {
                    Some(node) => match node.entry {
                        GrammarItem::NewVariable(name) => {
                            // TODO: Disable assigning units as variables
                            self.variable_register.set(name, value);
                            Value::empty(0.)
                        }
                        _ => unimplemented!(),
                    },
                    None => unimplemented!(),
                }
            }
            _ => ast.evaluate()?,
        };

        println!(" = {}", result.to_string());

        Ok(())
    }
}
