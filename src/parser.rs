use std::string::ToString;

use crate::lexer::{LexItem, Token};
use crate::unit::{Unit, UnitRegister};
use crate::value::Value;
use crate::variable::VariableRegister;
use crate::{CalcError, Result};

#[derive(Debug)]
pub enum GrammarItem {
    Assignment,
    Conversion,
    Equal,
    Plus,
    Minus,
    Times,
    Div,
    NewVariable(String),
    Value(Value),
    Unit(Unit),
}

#[derive(Debug)]
pub struct ParseNode {
    pub left: Option<Box<ParseNode>>,
    pub right: Option<Box<ParseNode>>,
    pub entry: GrammarItem,
}

impl ToString for ParseNode {
    fn to_string(&self) -> String {
        match &self.entry {
            GrammarItem::Conversion => format!(
                "{} -> {}",
                self.left.as_ref().unwrap().to_string(),
                self.right.as_ref().unwrap().to_string()
            ),
            GrammarItem::Plus => format!(
                "{} + {}",
                self.left.as_ref().unwrap().to_string(),
                self.right.as_ref().unwrap().to_string()
            ),
            GrammarItem::Minus => format!(
                "{} - {}",
                self.left.as_ref().unwrap().to_string(),
                self.right.as_ref().unwrap().to_string()
            ),
            GrammarItem::Times => format!(
                "{} * {}",
                self.left.as_ref().unwrap().to_string(),
                self.right.as_ref().unwrap().to_string()
            ),
            GrammarItem::Div => format!(
                "{} / {}",
                self.left.as_ref().unwrap().to_string(),
                self.right.as_ref().unwrap().to_string()
            ),
            GrammarItem::Assignment => format!(
                "{} = {}",
                self.left.as_ref().unwrap().to_string(),
                self.right.as_ref().unwrap().to_string()
            ),
            GrammarItem::Value(val) => val.to_string(),
            GrammarItem::Unit(unit) => unit.to_string(),
            GrammarItem::NewVariable(name) => name.to_string(),
            _ => format!("{:?}", self.entry),
        }
    }
}

impl ParseNode {
    pub fn new(entry: GrammarItem, left: Box<ParseNode>, right: Box<ParseNode>) -> ParseNode {
        ParseNode {
            left: Some(left),
            right: Some(right),
            entry,
        }
    }
    pub fn leaf(entry: GrammarItem) -> ParseNode {
        ParseNode {
            left: None,
            right: None,
            entry,
        }
    }
    pub fn evaluate(self) -> Result<Value> {
        match self.entry {
            GrammarItem::Conversion => match self.left {
                Some(node) => {
                    let val = node.evaluate()?;

                    match self.right {
                        Some(node) => match node.entry {
                            GrammarItem::Unit(unit) => {
                                if val.unit != unit {
                                    Err(CalcError::Error("Units are not equals".to_string()))
                                } else {
                                    Ok(val.to_unit(unit))
                                }
                            }
                            _ => Err(CalcError::Error(
                                "Right side of conversion is not a unit".to_string(),
                            )),
                        },
                        None => Err(CalcError::Error("No unit to convert to".to_string())),
                    }
                }
                None => Err(CalcError::Error(
                    "Found nothing left of conversion".to_string(),
                )),
            },
            GrammarItem::Plus => {
                if let None = self.right {
                    return Err(CalcError::Error(
                        "Right hand side of addition is empty".to_string(),
                    ));
                }

                let res = self.right.unwrap().evaluate()?;
                match self.left {
                    Some(node) => {
                        let left_res = node.evaluate()?;
                        Ok(left_res.add(res).unwrap())
                    }
                    None => Ok(res),
                }
            }
            GrammarItem::Minus => match self.left {
                Some(node) => {
                    let res = node.evaluate()?;

                    match self.right {
                        Some(right_node) => {
                            let right_res = right_node.evaluate()?;
                            match res.minus(right_res) {
                                Ok(val) => Ok(val),
                                err => err,
                            }
                        }
                        None => Err(CalcError::Error(
                            "No right side of substraction".to_string(),
                        )),
                    }
                }
                None => match self.right {
                    Some(node) => {
                        let mut res = node.evaluate()?;
                        res.imul(-1.);
                        Ok(res)
                    }
                    None => Err(CalcError::Error(
                        "Expected an expression on both side of the substraction".to_string(),
                    )),
                },
            },
            GrammarItem::Div => {
                if let None = self.right {
                    return Err(CalcError::Error(
                        "Right hand side of multiplication is empty".to_string(),
                    ));
                }
                if let None = self.left {
                    return Err(CalcError::Error(
                        "Left hand side of multiplication is empty".to_string(),
                    ));
                }

                let rr = self.right.unwrap().evaluate()?;
                let lr = self.left.unwrap().evaluate()?;

                match lr.div(rr) {
                    Ok(val) => Ok(val),
                    err => err,
                }
            }
            GrammarItem::Times => {
                if let None = self.right {
                    return Err(CalcError::Error(
                        "Right hand side of multiplication is empty".to_string(),
                    ));
                }
                if let None = self.left {
                    return Err(CalcError::Error(
                        "Left hand side of multiplication is empty".to_string(),
                    ));
                }

                let rr = self.right.unwrap().evaluate()?;
                let lr = self.left.unwrap().evaluate()?;

                match lr.mul(rr) {
                    Ok(val) => Ok(val),
                    err => err,
                }
            }
            GrammarItem::Value(val) => Ok(val),
            _ => Err(CalcError::Error("Node entry is wrong".to_string())),
        }
    }
}

pub fn parse(
    variable_register: &VariableRegister,
    unit_register: &UnitRegister,
    tokens: &[Token],
) -> Result<ParseNode> {
    for (i, token) in tokens.iter().enumerate() {
        if let LexItem::Equal = token.item {
            let left = parse_variable(unit_register, &tokens[..i])?;
            let right = parse_conversand(variable_register, unit_register, &tokens[i + 1..])?;

            return Ok(ParseNode::new(
                GrammarItem::Assignment,
                Box::new(left),
                Box::new(right),
            ));
        }
    }
    parse_conversand(variable_register, unit_register, tokens)
}

fn parse_variable(unit_register: &UnitRegister, tokens: &[Token]) -> Result<ParseNode> {
    match tokens.len() {
        1 => match &tokens[0].item {
            LexItem::Symbol(name) => {
                if let Some(_) = unit_register.get(name) {
                    Err(CalcError::PlacedError(
                        format!("The name {} is reserved", name),
                        tokens[0].start,
                        tokens[0].end,
                    ))
                } else {
                    Ok(ParseNode::leaf(GrammarItem::NewVariable(name.to_string())))
                }
            }
            _ => Err(CalcError::PlacedError(
                format!("Expected variable name but got {:?}", tokens[0].item),
                tokens[0].start,
                tokens[0].end,
            )),
        },
        _ => Err(CalcError::Error(
            "Left side of assignment is not a valid variable name".to_string(),
        )),
    }
}

fn parse_conversand(
    variable_register: &VariableRegister,
    unit_register: &UnitRegister,
    tokens: &[Token],
) -> Result<ParseNode> {
    for (i, token) in tokens.iter().enumerate() {
        if let LexItem::Conversion = token.item {
            let left = parse_addand(variable_register, unit_register, &tokens[..i])?;
            let right = parse_unit(unit_register, &tokens[i + 1..])?;
            let right = ParseNode::leaf(GrammarItem::Unit(right));

            return Ok(ParseNode::new(
                GrammarItem::Conversion,
                Box::new(left),
                Box::new(right),
            ));
        }
    }
    parse_addand(variable_register, unit_register, tokens)
}

fn parse_divand(
    variable_register: &VariableRegister,
    unit_register: &UnitRegister,
    tokens: &[Token],
) -> Result<ParseNode> {
    for (i, token) in tokens.iter().enumerate() {
        if let LexItem::Op('/') = token.item {
            let up = parse_value(variable_register, unit_register, &tokens[..i])?;
            if let GrammarItem::Unit(..) = up.entry {
                continue;
            }
            let down = parse_divand(variable_register, unit_register, &tokens[i + 1..])?;
            if let GrammarItem::Unit(..) = down.entry {
                // Denominator is a unit, continue
                continue;
            }

            return Ok(ParseNode::new(
                GrammarItem::Div,
                Box::new(up),
                Box::new(down),
            ));
        }
    }
    parse_value(variable_register, unit_register, tokens)
}

fn parse_muland(
    variable_register: &VariableRegister,
    unit_register: &UnitRegister,
    tokens: &[Token],
) -> Result<ParseNode> {
    for (i, token) in tokens.iter().enumerate() {
        if let LexItem::Op('*') = &token.item {
            let left = parse_divand(variable_register, unit_register, &tokens[..i])?;
            let right = parse_muland(variable_register, unit_register, &tokens[i + 1..])?;
            if let GrammarItem::Unit(..) = right.entry {
                continue;
            }

            return Ok(ParseNode::new(
                GrammarItem::Times,
                Box::new(left),
                Box::new(right),
            ));
        }
    }
    parse_divand(variable_register, unit_register, tokens)
}

fn parse_minand(
    variable_register: &VariableRegister,
    unit_register: &UnitRegister,
    tokens: &[Token],
) -> Result<ParseNode> {
    for (i, token) in tokens.iter().enumerate() {
        if let LexItem::Op('-') = &token.item {
            let left = parse_muland(variable_register, unit_register, &tokens[..i])?;
            let right = parse_minand(variable_register, unit_register, &tokens[i + 1..])?;

            return Ok(ParseNode::new(
                GrammarItem::Minus,
                Box::new(left),
                Box::new(right),
            ));
        }
    }
    parse_muland(variable_register, unit_register, tokens)
}

fn parse_addand(
    variable_register: &VariableRegister,
    unit_register: &UnitRegister,
    tokens: &[Token],
) -> Result<ParseNode> {
    for (i, token) in tokens.iter().enumerate() {
        if let LexItem::Op('+') = &token.item {
            let left = parse_minand(variable_register, unit_register, &tokens[..i])?;
            let right = parse_addand(variable_register, unit_register, &tokens[i + 1..])?;

            return Ok(ParseNode::new(
                GrammarItem::Plus,
                Box::new(left),
                Box::new(right),
            ));
        }
    }
    parse_minand(variable_register, unit_register, tokens)
}

fn parse_value(
    variable_register: &VariableRegister,
    unit_register: &UnitRegister,
    tokens: &[Token],
) -> Result<ParseNode> {
    if tokens.len() == 0 {
        return Err(CalcError::Error(String::from("Can't parse value")));
    }

    match &tokens[0].item {
        LexItem::Number(num) => {
            let unit = parse_unit(unit_register, &tokens[1..])?;
            let val = Value::new(*num as f64, unit);
            Ok(ParseNode::leaf(GrammarItem::Value(val)))
        }
        LexItem::Symbol(name) => {
            let value = variable_register.get(name.to_string());
            match value {
                Some(val) => Ok(ParseNode::leaf(GrammarItem::Value(val))),
                None => match parse_unit(unit_register, tokens) {
                    Ok(unit) => Ok(ParseNode::leaf(GrammarItem::Unit(unit))),
                    Err(..) => Err(CalcError::PlacedError(
                        format!("Variable {} not found", name),
                        tokens[0].start,
                        tokens[0].end,
                    )),
                },
            }
        }
        // This is not a value or a variable, parse as a unit instead
        _ => {
            let unit = parse_unit(unit_register, tokens)?;
            Ok(ParseNode::leaf(GrammarItem::Unit(unit)))
        }
    }
}

fn parse_unit(unit_register: &UnitRegister, tokens: &[Token]) -> Result<Unit> {
    if tokens.len() == 0 {
        return Ok(Unit::Empty);
    }

    if tokens.len() == 1 {
        if let LexItem::Symbol(symbol) = &tokens[0].item {
            return match unit_register.get(&symbol) {
                Some(unit) => Ok(unit),
                None => Err(CalcError::PlacedError(
                    format!("Can't find unit {}", &symbol),
                    tokens[0].start,
                    tokens[0].end,
                )),
            };
        }
    }

    for (i, token) in tokens.iter().enumerate() {
        if let LexItem::Op('/') = &token.item {
            let left = parse_unit(unit_register, &tokens[..i])?;
            if let Unit::Empty = left {
                return Err(CalcError::PlacedError(
                    "No unit found left of the division".to_string(),
                    token.start,
                    token.end,
                ));
            }
            let right = parse_unit(unit_register, &tokens[i + 1..])?;
            if let Unit::Empty = right {
                return Err(CalcError::PlacedError(
                    "No unit found right of the division".to_string(),
                    token.start,
                    token.end,
                ));
            }
            return Ok(Unit::div(left, right));
        }
    }

    Err(CalcError::Error("unit not found".to_string()))
}

#[test]
fn test_parse_value() -> Result<()> {
    let tokens = vec![
        Token {
            item: LexItem::Number(100),
            start: 0,
            end: 3,
        },
        Token {
            item: LexItem::Symbol("m".to_string()),
            start: 3,
            end: 4,
        },
        Token {
            item: LexItem::Op('/'),
            start: 4,
            end: 5,
        },
        Token {
            item: LexItem::Symbol("s".to_string()),
            start: 3,
            end: 4,
        },
    ];
    let unit_register = UnitRegister::new();
    let variable_register = VariableRegister::new();
    let mps = Unit::Div(
        Box::new(Unit::Standard("m".to_string())),
        Box::new(Unit::Standard("s".to_string())),
    );
    let expected_value = Value::new(100., mps);

    let ast = parse(&variable_register, &unit_register, &tokens);
    match ast {
        Ok(node) => match node.entry {
            GrammarItem::Value(value) => {
                if expected_value != value {
                    Err(CalcError::Error(format!(
                        "Expected 100 m/s but got {:?}",
                        value
                    )))
                } else {
                    Ok(())
                }
            }
            _ => Err(CalcError::Error(format!(
                "Expected a value, but got {:?}",
                node
            ))),
        },
        Err(err) => Err(err),
    }
}

#[test]
fn test_parse_div_op() -> Result<()> {
    let tokens = vec![
        Token {
            item: LexItem::Number(100),
            start: 0,
            end: 3,
        },
        Token {
            item: LexItem::Op('/'),
            start: 3,
            end: 4,
        },
        Token {
            item: LexItem::Number(100),
            start: 4,
            end: 5,
        },
    ];
    let unit_register = UnitRegister::new();
    let variable_register = VariableRegister::new();
    let expected_value = Value::new(100., Unit::Empty);

    let ast = parse(&variable_register, &unit_register, &tokens);
    match ast {
        Ok(node) => match node.entry {
            GrammarItem::Div => match node.left {
                Some(left) => match *left {
                    ParseNode {
                        entry: GrammarItem::Value(left_value),
                        ..
                    } => {
                        if expected_value != left_value {
                            Err(CalcError::Error(format!(
                                "Expected value of 100 but got {:?}",
                                left_value
                            )))
                        } else {
                            Ok(())
                        }
                    }
                    _ => Err(CalcError::Error(format!(
                        "Expected value of 100. but got {:?}",
                        *left
                    ))),
                },
                _ => Err(CalcError::Error(format!(
                    "Got {:?} as left value for div",
                    node.left
                ))),
            },
            _ => Err(CalcError::Error(format!("Expected div but got {:?}", node))),
        },
        Err(err) => Err(err),
    }
}
