use std::collections::HashMap;

use crate::value::Value;

pub struct VariableRegister {
    variables: HashMap<String, Value>,
}

impl VariableRegister {
    pub fn new() -> VariableRegister {
        let mut variables = HashMap::new();
        variables.insert("pi".to_string(), Value::empty(3.1415926535));
        VariableRegister { variables }
    }

    pub fn set(&mut self, name: String, value: Value) {
        self.variables.insert(name, value);
    }

    pub fn get(&self, name: String) -> Option<Value> {
        self.variables.get(&name).cloned()
    }
}
