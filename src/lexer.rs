use crate::{CalcError, Result};
use std::iter::Peekable;

#[derive(Debug)]
pub enum LexItem {
    Number(i64),
    Equal,
    Unit(String),
    Paren(char),
    Op(char),
    Symbol(String),
    Conversion,
}

#[derive(Debug)]
pub struct Token {
    pub item: LexItem,
    pub start: usize,
    pub end: usize,
}

impl Token {
    pub fn new(item: LexItem, start: usize, end: usize) -> Token {
        Token { item, start, end }
    }
}

pub fn lex(input: &String) -> Result<Vec<Token>> {
    let mut result = Vec::new();

    let mut pos = 0;
    let mut it = input.chars().peekable();
    while let Some(&c) = it.peek() {
        match c {
            '0'..='9' => {
                it.next();
                let (n, d) = get_number(c, &mut it);
                result.push(Token {
                    item: LexItem::Number(n),
                    start: pos,
                    end: pos + d,
                });
                pos += d;
            }
            '-' => {
                it.next();
                if let Some('>') = it.peek() {
                    result.push(Token {
                        item: LexItem::Conversion,
                        start: pos,
                        end: pos + 2,
                    });
                    it.next();
                    pos += 2;
                } else {
                    result.push(Token {
                        item: LexItem::Op('-'),
                        start: pos,
                        end: pos + 1,
                    });
                    pos += 1;
                }
            }
            '+' | '*' | '/' => {
                result.push(Token {
                    item: LexItem::Op(c),
                    start: pos,
                    end: pos + 1,
                });
                it.next();
                pos += 1;
            }
            '(' | '[' => {
                result.push(Token {
                    item: LexItem::Paren(c),
                    start: pos,
                    end: pos + 1,
                });
                it.next();
                pos += 1;
            }
            ')' | ']' => {
                result.push(Token {
                    item: LexItem::Paren(c),
                    start: pos,
                    end: pos + 1,
                });
                it.next();
                pos += 1;
            }
            ' ' => {
                pos += 1;
                it.next();
            }
            'A'..='Z' | 'a'..='z' => {
                it.next();
                let (s, d) = get_symbol(c, &mut it);
                result.push(Token {
                    item: LexItem::Symbol(s),
                    start: pos,
                    end: pos + d,
                });
                pos += d;
            }
            '=' => {
                it.next();
                result.push(Token {
                    item: LexItem::Equal,
                    start: pos,
                    end: pos + 1,
                });
                pos += 1;
            }
            _ => {
                return Err(CalcError::PlacedError(
                    format!("unexpected character {}", c),
                    pos,
                    pos + 1,
                ));
            }
        }
    }
    Ok(result)
}

fn get_symbol<T: Iterator<Item = char>>(c: char, iter: &mut Peekable<T>) -> (String, usize) {
    let mut symbol = c.to_string();
    let mut delta = 1;
    while let Some(c) = iter.peek() {
        match c {
            'A'..='Z' | 'a'..='z' => {
                symbol.push(*c);
                iter.next();
                delta += 1;
            }
            _ => break,
        }
    }
    (symbol, delta)
}

fn get_number<T: Iterator<Item = char>>(c: char, iter: &mut Peekable<T>) -> (i64, usize) {
    let mut number = c
        .to_string()
        .parse::<i64>()
        .expect("The caller should have passed a digit");
    let mut delta = 1;
    while let Some(Ok(digit)) = iter.peek().map(|c| c.to_string().parse::<i64>()) {
        number = number * 10 + digit;
        delta += 1;
        iter.next();
    }
    (number, delta)
}

#[test]
fn test_lex_number() -> Result<()> {
    let input = String::from("12142");
    let tokens = lex(&input)?;
    let first_token = &tokens[0];

    assert_eq!(first_token.start, 0);
    assert_eq!(first_token.end, 5);
    match first_token.item {
        LexItem::Number(12142) => Ok(()),
        _ => Err(CalcError::PlacedError(
            format!("Expected Number(12142) but found {:?}", first_token.item),
            0,
            6,
        )),
    }
}

#[test]
fn test_lex_symbol() -> Result<()> {
    let input = String::from("aaaaa");
    let tokens = lex(&input)?;
    let first_token = &tokens[0];

    assert_eq!(first_token.start, 0);
    assert_eq!(first_token.end, 5);
    match &first_token.item {
        LexItem::Symbol(symbol) => {
            if symbol == "aaaaa" {
                Ok(())
            } else {
                Err(CalcError::PlacedError(
                    format!("Expected Symbol(aaaaa) but got Symbol({})", symbol),
                    0,
                    5,
                ))
            }
        }
        _ => Err(CalcError::PlacedError(
            format!("Expected Symbol(aaaaa) but found {:?}", first_token.item),
            0,
            5,
        )),
    }
}
