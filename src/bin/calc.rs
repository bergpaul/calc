use clap::{App, Arg};
use colorful::{Color, Colorful};
use rustyline::error::ReadlineError;
use rustyline::Editor;

use calc::{CalcError, Interpreter};

fn print_error(error: CalcError, padding: usize) {
    match error {
        CalcError::Error(msg) => {
            let msg = format!("Error: {}", msg);
            println!("{}", msg.color(Color::Red));
        }
        CalcError::PlacedError(msg, start, end) => {
            let msg = format!("Error: {}", msg);
            println!(
                "{}",
                format!(
                    "{:>start$}{:^>length$}",
                    "",
                    "^",
                    start = start + padding,
                    length = end - start
                )
                .color(Color::Red)
            );
            println!("{}", msg.color(Color::Red));
        }
    };
}

fn repl() {
    let mut rl = Editor::<()>::new();
    let mut interpreter = Interpreter::new();
    loop {
        let readline = rl.readline("> ");
        match readline {
            Ok(line) => {
                let res = interpreter.step(&line);
                if let Err(err) = res {
                    print_error(err, 2);
                }
            }
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
}

fn main() {
    let matches = App::new("Calc")
        .about("Do operations with unit support")
        .arg(
            Arg::with_name("code")
                .short("c")
                .long("code")
                .help("Evaluate in one line")
                .takes_value(true),
        )
        .get_matches();
    let eval = matches.value_of("code");
    match eval {
        Some(code) => {
            let mut interpreter = Interpreter::new();
            let res = interpreter.step(&code.to_string());
            if let Err(err) = res {
                println!("{}", code);
                print_error(err, 0);
            }
        }
        None => repl(),
    }
}
